package domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tab_salgado")
public class Salgado extends Produto {

	@Column(nullable = false, columnDefinition = "int")
	private TipoSalgadoEnum tipoSalgadoEnum;

	public Salgado() {
		super();
	}

	public Salgado(TipoSalgadoEnum tipoSalgadoEnum) {
		super();
		this.tipoSalgadoEnum = tipoSalgadoEnum;
	}

	public TipoSalgadoEnum getTipoSalgadoEnum() {
		return tipoSalgadoEnum;
	}

	public void setTipoSalgadoEnum(TipoSalgadoEnum tipoSalgadoEnum) {
		this.tipoSalgadoEnum = tipoSalgadoEnum;
	}

}
