package domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tab_cliente")
public class Cliente {

	@Id
	@Column(columnDefinition = "char(11)", nullable = false)
	private String cpf;
	@Column(columnDefinition = "char(40)", nullable = false)
	private String nome;

	@OneToMany(targetEntity = Endereco.class, 
			fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Endereco> enderecos;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "tab_produto_cliente", joinColumns = @JoinColumn(name = "codigo"), inverseJoinColumns = @JoinColumn(name = "cpf"))
	private List<Produto> produtos;

	public Cliente() {
	}

	public Cliente(String cpf, String nome, List<Endereco> enderecos, List<Produto> produtos) {
		super();
		this.cpf = cpf;
		this.nome = nome;
		this.enderecos = enderecos;
		this.produtos = produtos;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Endereco> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}

	public List<Produto> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

	@Override
	public String toString() {
		return "Cliente [cpf=" + cpf + ", nome=" + nome + ", enderecos=" + enderecos + ", produtos=" + produtos + "]";
	}

	
	
}
