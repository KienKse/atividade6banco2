package domain;

public enum TipoSalgadoEnum {

	ASSADO(13), FRITO(23), CONGELADO(34);

	private TipoSalgadoEnum(Integer codigo) {
		this.codigo = codigo;
	}

	private Integer codigo;

	public Integer getCodigo() {
		return codigo;
	}

	public static TipoSalgadoEnum valorOfCodigo(Integer codigo) {
		for (TipoSalgadoEnum situacaoVendedorEnum : values()) {
			if (situacaoVendedorEnum.codigo.equals(codigo)) {
				return situacaoVendedorEnum;
			}
		}
		throw new IllegalArgumentException("Codigo n�o encontrado =" + codigo);
	}

}
