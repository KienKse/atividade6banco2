package exemplo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import domain.Cliente;
import domain.Endereco;
import domain.Produto;
import domain.Salgado;
import domain.TipoSalgadoEnum;

public class Exemplo1 {

	private static Cliente c1 = null;
	private static Cliente c2 = null;

	public static void main(String[] args) {
		EntityManagerFactory emf = null;
		try {

			emf = Persistence.createEntityManagerFactory("vendaSalgados");
			EntityManager em = emf.createEntityManager();

			// 2 produto (1 salgado e outro gen�rico)

			/**
			 * 7. Utilizando JPA, fa�a a inclus�o de 2 produto (1 salgado e
			 * outro gen�rico) e 2 clientes. (crie um m�todo espec�fico para
			 * esta a��o)
			 */
			em.getTransaction().begin();

			Salgado s1 = new Salgado(TipoSalgadoEnum.CONGELADO);
			definirObjetosProduto(s1, "Empada", 3.0, 2);
			Produto p1 = new Produto("Alvejante", 5.0, 1);

			em.persist(s1);
			em.persist(p1);

			// 2 clientes. (crie um m�todo espec�fico para esta a��o)
			inserirDoisClientes(em);

			em.getTransaction().commit();

			/**
			 * 8. Utilizando JPA, fa�a a exclus�o de 1 dos clientes. (crie um
			 * m�todo espec�fico para esta a��o)
			 */
			removerCliente(em);
			
			/**
			 9. Utilizando JPA, fa�a a atualiza��o do nome do cliente que n�o foi exclu�do.
			  (crie um m�todo espec�fico para esta a��o)
			 */
			
			atualizarNomeCliente(em);

			em.close();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.print("Falha no programa principal:" + e);
		} finally {
			if (emf != null) {
				emf.close();
			}
		}
	}

	private static void atualizarNomeCliente(EntityManager em) {
		em.getTransaction().begin();
		
		System.out.println("********************* Cliente *******************************");
		String jpql = "select c from Cliente c where c.nome = :nome";
		TypedQuery<Cliente> query = em.createQuery(jpql, Cliente.class);
		query.setParameter("nome", "Ana");
		Cliente cliente = query.getSingleResult();
		
		System.out.println(cliente.toString());
		
		cliente.setNome("Creuza");
		
		em.persist(cliente);
		
		em.getTransaction().commit();
	}
	
	private static void executarConsultaClientes(EntityManager em) {
		System.out.println("********************* Clientes *******************************");
		String jpql = "select c from Cliente c";
		TypedQuery<Cliente> query = em.createQuery(jpql, Cliente.class);
		List<Cliente> clientes = query.getResultList();
		for (Cliente cliente : clientes) {
			System.out.println(cliente.toString());
		}
	}

	private static void removerCliente(EntityManager em) {
		em.getTransaction().begin();
		System.out.println("CLIENTES LISTA COMPLETA: ");
		executarConsultaClientes(em);

		em.remove(c1);

		em.getTransaction().commit();

		System.out.println("CLIENTES LISTA REMOVED: ");
		executarConsultaClientes(em);
	}

	private static void inserirDoisClientes(EntityManager em) {
		c1 = new Cliente("11111111111", "Amanda", new ArrayList<Endereco>(), new ArrayList<Produto>());
		c2 = new Cliente("22222222222", "Ana", new ArrayList<Endereco>(), new ArrayList<Produto>());

		em.persist(c1);
		em.persist(c2);
	}

	private static void definirObjetosProduto(Salgado salgado, String string, double d, int i) {
		salgado.setNome(string);
		salgado.setValorUnitario(d);
		salgado.setQtdMinima(i);
	}

}
